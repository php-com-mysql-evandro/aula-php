<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
</head>

<h2>Envio por GET</h2>

<body>
    <form action="aula6.php" method="get">
        <p>
            <label for="">Nome:</label>
            <input type="text" name="nome">
        </p>
        <p>
            <label for="">Idade:</label>
            <input type="text" name="idade">
        </p>
        <p>
            <button type="submit">Cadastro</button>
        </p>
    </form>

    <?php
        if (isset($_GET['nome'])) echo $_GET['nome']."<br>";
        if (isset($_GET['idade'])) echo $_GET['idade']."<br>";
    ?>

</body>

<h2>Envio por POST</h2>

<body>
    <form action="aula6.php" method="post">
        <p>
            <label for="">Nome:</label>
            <input type="text" name="nome">
        </p>
        <p>
            <label for="">Idade:</label>
            <input type="text" name="idade">
        </p>
        <p>
            <button type="submit">Cadastro</button>
        </p>
    </form>

    <?php
        if (isset($_POST['nome'])) echo $_POST['nome']."<br>";
        if (isset($_POST['idade'])) echo $_POST['idade']."<br>";
    ?>

</body>

</html>