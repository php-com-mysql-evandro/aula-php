<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Exemplo de Javascript</title>
    </head>
    <body>

        <a href="javascript:;" id="btnAlerta">Mostrar Alerta em Javascript</a>

        <br>

        <a href="https://www.google.com.br" target="_blank">Google</a>

        <script>

            var alerta = document.getElementById("btnAlerta");
            alerta.onclick = function(){
                alert('Este é um exemplo de Javascript')
            }

        </script>
    </body>
</html>