<?php 
echo "<h1>Estrutura condicional</h1>";

$nome='Evandro';
$idade=24;
$email='evandro.hsakuma@senacsp.edu.br';
$senha="1597536842";

//////////////////////////////////

echo "<h4>...if()...</h4>";

if($idade>=18){
    echo "O usuário $nome é maior de idade";
}

echo "<hr>";
//////////////////////////////////

echo "<h4>...if() e else...</h4>";

if($idade>=18){
    echo "O usuário $nome é maior de idade";
}else{
    echo "O usuário $nome é menor de idade";
}

echo "<hr>";
//////////////////////////////////

echo "<h4>Exemplo de Consição para Login</h4>";

if($email == "evandro.hsakuma@senacsp.edu.br" && $senha == "1597536842"){
    echo "Usuário Logado";
}else{
    echo "Usuário ou Senha Inválido";
}

echo "<hr>";
//////////////////////////////////

echo "<h4>...if() else elseif() else...</h4>";

$idade = 24;

if($idade>=18){
    echo "O usuário $nome é Adulto";
}elseif($idade>=12){
    echo "O usuário $nome é Adolecente";
}else{
    echo "O usuário $nome é Criança";
}

echo "<hr>";
//////////////////////////////////

echo "<h4>...if() ternario...</h4>";

echo ($idade>=18)?"O usuário $nome é maior de idade":"O usuário $nome é menor de idade";

echo "<hr>";
//////////////////////////////////

echo "<h4>condição com switch case</h4>";

$menu = 'Escol';

switch ($menu) {
    case 'Home':
        echo 'Clicou no menu Home';
        break;
    
    case 'Empresa':
        echo 'Clicou no menu Empresa';
        break;
        
    case 'Escola':
        echo 'Clicou no menu Escola';
        break;
    
    case 'Contato':
        echo 'Clicou no menu Contato';
        break;
    default:
        echo 'Nenhuma opção selecionado';
        break;
}
