<?php
    $listaCompra = ["banana","arroz","feijão"];

    echo $listaCompra[1]."<hr>";
    
    echo "<pre>";
    print_r($listaCompra);
    echo "</pre><hr>";

    $usuarios = [
        [
            "nome" => "Evandro Horiuti Sakuma",
            "e-mail" => "evandro.hsakuma@senacsp.edu.br"
        ],
        [
            "nome" => "Matias",
            "e-mail" => "matias@senacsp.edu.br"
        ],
        [
            "nome" => "Evandro Horiuti Sakuma",
            "e-mail" => "evandro.hsakuma@senacsp.edu.br"
        ],
    ];

    echo "<pre>";
    print_r($usuarios);
    echo "</pre><hr>";
    
    echo $usuarios[0]["nome"]."<hr>";

    foreach($listaCompra as $item){
        echo $item.", ";
    }

    echo "<hr>";

    foreach ($usuarios as $u) {
        foreach ($u as $d){
            echo $d.'<br>';
        }
    }
?>