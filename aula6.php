<?php
    echo '<h1>Laço de Repetição</h1>';
    echo '<h2>while</h2>';

    $cont = 1;
    while ($cont<=10){
        echo $cont.' ';
        $cont ++;
    }

    echo '<h2><hr>for</h2>';

    for ($i=1; $i <= 10; $i++) { 
        echo $i.' ';
    }

    echo '<h2><hr>do while</h2>';

    $cont = 1;
    do{
        echo $cont.' ';
        $cont ++;
    }while ($cont<=10);
    
?>